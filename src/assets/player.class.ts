import {Vector2D} from '../game-engine/vector-2d.class';
import {GameObject} from '../game-engine/game-object.class';
import {AnimationComponent} from '../game-engine/components/animation.component';
import {GameComponentType} from '../game-engine/components/game-component-type.enum';
import {MovementComponent} from '../game-engine/components/movement.component';
import {PlayerMenu} from "../models/PlayerMenu.class";
import {Character} from "../models/Character.class";

const frameWidth = 32;
const frameHeight = 32;

/**
 * @class Player
 * @requires AnimationComponent
 */
export class Player extends GameObject {
  // shortcut to get the animation component.
  animationComponent: AnimationComponent | undefined;

  // shortcut to get the animation component.
  movementComponent: MovementComponent | undefined;

  playerMenu: PlayerMenu;
  playerGameId: number;
  character: Character | undefined = undefined;
  range: number = 4;
  currentPv: number = 0;
  gameOrder: number; // start to 1
  isSelected: boolean = false;
  hasMove: boolean = false;
  hasSmash: boolean = false;
  endPosition: number|undefined = undefined;

  /**
   * @constructor
   * Construct a Player instance.
   */
  constructor(position: Vector2D, playerMenu: PlayerMenu, playerGameId: number, character: Character | undefined, gameOrder: number, endPosition: number|undefined = undefined) {
    super(position);
    this.position = position;
    this.gameOrder = gameOrder;
    this.playerMenu = playerMenu;
    this.playerGameId = playerGameId;

    if(character != null){
      this.character = character;
      this.currentPv = this.character.hp;
      this.range = this.character.range;
    }

    this.endPosition = endPosition;
  }

  /**
   * Draw the current player frame, in function of the time passed.
   * @param {CanvasRenderingContext2D} ctx Context 2D of the canvas the player should be drawned
   * @param {Vector2D} cellPixelSize the cell pixel size. Get by grid getCellPixelSize
   * @param {number} time time passed in ms
   */
  draw(ctx: CanvasRenderingContext2D, cellPixelSize: Vector2D, time: number): void {
    if(this.currentPv <=0) { // player is dead
      return;
    }
    ctx.imageSmoothingEnabled = false;
    const animationEntry = this.animationComponent!.currentAnimation.getSpriteAnimationEntryFromTime(time);
    if (this.playerMenu._isYou) {
      ctx.save();
      ctx.fillStyle = 'rgba(255, 255, 0, 0.6)';
      ctx.fillRect(
        this.position.x * cellPixelSize.x,
        this.position.y * cellPixelSize.y,
        cellPixelSize.x,
        cellPixelSize.y,
      )
      ctx.restore();
    }
    ctx.drawImage(
      this.animationComponent!.spriteSheet!,
      animationEntry?.col * frameWidth,
      animationEntry?.row * frameHeight,
      frameWidth,
      frameHeight,
      this.position.x * cellPixelSize.x - cellPixelSize.x / 2,
      (this.position.y - 1) * cellPixelSize.y,
      cellPixelSize.x * 2,
      cellPixelSize.y * 2
    );
    // ctx.drawImage(sprite, 50, 50, 160, 128);
  }

  /**
   * @override
   */
  async start(): Promise<void> {
    super.start();
    this.animationComponent = this.getComponent<AnimationComponent>(GameComponentType.AnimationComponent);
    this.movementComponent = this.getComponent<MovementComponent>(GameComponentType.MovementComponent);
  }

  update(time: number): void {

  }

  move(positionX: number, positionY: number) {
    this.position = new Vector2D(positionX, positionY)
  }

  teleportation(x: number, y: number) {
    if (!this.hasMove) {
      this.hasMove = !this.hasMove;
      this.movementComponent!.move(x, y);
    }
  }

  smash(playerToSmash: Player) {
    if (!this.hasSmash) {
      this.hasSmash = !this.hasSmash;
      playerToSmash.currentPv -= this.character!.attack;
    }

  }
}