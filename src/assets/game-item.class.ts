import {GameObject} from "../game-engine/game-object.class";
import {Player} from "./player.class";
import {Vector2D} from "../game-engine/vector-2d.class";
import {Item} from "../models/Item.class";

export class GameItem extends GameObject {

  item: Item
  player: Player|null;

  constructor(position: Vector2D, item: Item) {
    super(position);
    this.player = null;
    this.item = item;

  }

  draw(ctx: CanvasRenderingContext2D, cellPixelSize: Vector2D, time: number): void {
    if(this.player == null) { // Item is getting ?
      // draw item
    }
  }

  update(time: number): void {
  }
  
  setToPlayer(player: Player) {
    this.player = player;
  }

}