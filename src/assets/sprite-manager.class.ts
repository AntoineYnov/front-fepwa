export class SpriteManager {

  private static instance: SpriteManager | null = null;

  public static getInstance(): SpriteManager {
    if (!SpriteManager.instance) {
      SpriteManager.instance = new SpriteManager();
    }

    return SpriteManager.instance;
  }

  public getSpriteFromUri(uri: string): HTMLImageElement {
    const sprite = new Image();
    sprite.src = uri;
    return sprite;
  };
}