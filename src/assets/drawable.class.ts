import { Vector2D } from '../game-engine/vector-2d.class';

export abstract class Drawable {
  position: Vector2D;
  abstract draw(ctx: CanvasRenderingContext2D, time: number): void;

  constructor(position: Vector2D) {
    this.position = position;
  }
}