export function timeout(ms: number): Promise<void> {
  return new Promise<void>((resolve) => setTimeout(resolve, ms));
}

export function getRandomInt(max:number) {
  return Math.floor(Math.random() * Math.floor(max));
}