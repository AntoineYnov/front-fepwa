import { ApiService } from "./api.service";
import { PlayerMenu, IPlayerFromServer } from "../models/PlayerMenu.class";

export class PlayerService {

    apiService : ApiService =  ApiService.getInstance();

    private static instance: PlayerService;
    
    /**
     * Singleton method to get the only one instance of the class
     */
    public static getInstance(): PlayerService {
        if (!PlayerService.instance) {
            PlayerService.instance = new PlayerService();
        }

        return PlayerService.instance;
    }


    async getToken() {
        const tokenString = localStorage.getItem('authentication_token') || '{}';
        let userToken = JSON.parse(tokenString);
        if(tokenString === '{}') {
            userToken = await this.apiService.request('post','authenticate', {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            this.setToken(userToken);
        }
        return userToken?.authentication_token  
    }

    setToken(userToken : number) {
        localStorage.setItem('authentication_token', JSON.stringify(userToken));
    }

    /**
     * Get the authenticated player informations
     * @return {Promise<PlayerMenu>} the player informations
     */
    async getAuthenticatedPlayer(): Promise<PlayerMenu> {
        const tokenString = localStorage.getItem('authentication_token') || JSON.stringify({ authentication_token: ''});
        let token = JSON.parse(tokenString);
        const playerInfos = await this.apiService.request('get', 'players/myself', { headers: { 'Authentication-Token': token.authentication_token } }) as IPlayerFromServer
        return PlayerMenu.fromServer(playerInfos, true);
    }

    /**
     * Update the player infos
     * @param playerUpdateBody body of the request
     * @return {Promise<void>}
     */
    async updatePlayer(playerUpdateBody: { pseudo: string, picture_url: string }): Promise<void> {
        const { _id } = await this.getAuthenticatedPlayer();
        const { pseudo, picture_url } = playerUpdateBody;

        const tokenString = localStorage.getItem('authentication_token') || JSON.stringify({ authentication_token: ''});
        let token = JSON.parse(tokenString);
        
        this.apiService.request('put', `players/${_id}`,  { body: { pseudo, picture_url }, headers: { 'Authentication-Token': token.authentication_token } })
    }

}