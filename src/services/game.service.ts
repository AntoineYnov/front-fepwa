
import { ApiService } from "./api.service";
import { getToken } from "../common/utils/common.utils";
import { Game, IGameFromServer, IGameStatsFromServer } from "../models/Game.class";
import { ICharacterFromServer } from "../models/Character.class";

export interface IGameUpdateBody {
  player_games_update: IPlayerGameUpdate[],
  player_number: number,
  tour_number: number,
}

export interface IPlayerGameUpdate {
  id: number,
  player: number,
  game: number,
  x: number,
  y: number,
  lasting_pv: number,
  character: ICharacterFromServer,
  position_in_game: number,
}

export class GameService {

  apiService = ApiService.getInstance();

  private static instance: GameService;

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): GameService {
    if (!GameService.instance) {
      GameService.instance = new GameService();
    }

    return GameService.instance;
  }

  async startGame(slug: string): Promise<void> {
    const token = getToken();
    await this.apiService.request('post', `games/start/${slug}`, { headers: { 'Authentication-Token': token } })
  }

  async closeGame(slug: string): Promise<void> {
    const token = getToken();
    await this.apiService.request('post', `games/close/${slug}`, { headers: { 'Authentication-Token': token } })
  }

  async leaveGame(slug: string): Promise<void> { 
    const token = getToken();
    await this.apiService.request('post', `games/leave/${slug}`, { headers: { 'Authentication-Token': token } })
  }

  async getGame(slug: string): Promise<Game> {
    const token = getToken();
    const gameFromServer = await this.apiService.request('get', `games/by-slug/${slug}`, { headers: { 'Authentication-Token': token } }) as { game: IGameFromServer };
    return Game.fromServer(gameFromServer.game);
  }

  async updateGame(gameId: number, gameUpdateBody: IGameUpdateBody): Promise<void> {
    await this.apiService.request('put', `games/${gameId}`, { body: gameUpdateBody })
  }

  async getPlayerGame(): Promise<Game[]> {
    const serverGames = await this.apiService.request('get','games/my-games') as IGameFromServer[]
    return Promise.all(serverGames.map((serverGame) => Game.fromServer(serverGame)));
  }

  async getGameStats(slug: string): Promise<IGameStatsFromServer> {
    return this.apiService.request('get', `games/stats/${slug}`) as Promise<IGameStatsFromServer>;
  }

}