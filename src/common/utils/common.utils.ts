/**
 * Group all utils function that can be not grouped with some other functions
 */

/**
 * A promise to wait some time
 * @param {number} ms time to wait
 * @return {Promise<void>}
 */
export function timeout(ms: number): Promise<void> {
    return new Promise<void>((res) => setTimeout(res, ms));
  }
  
  /**
   * Function from mongoose utils. Merge an object containing default values
   * with the same object containing 
   * @param {T} defaults default values for the options 
   * @param {T} options the custom values to merge with the defaults 
   * @return {T} the merged options
   */
  export function mergeOptions<T>(defaults: T, options: T): T {
    const keys = Object.keys(defaults);
    let i = keys.length;
    let k: string;
    
    const mergedOptions: T = options || {} as T;
  
    while (i--) {
      k = keys[i];
      if (!(k in mergedOptions)) {
        mergedOptions[k as keyof T] = defaults[k as keyof T];
      }
    }
  
    return mergedOptions;
  };
  
  export function isEmpty(obj :any) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
  
    return true;
  }

export function urlBase64ToUint8Array(base64String: string): Uint8Array {
    const padding: string = "=".repeat((4-base64String.length%4)%4);
    const base64: string = (base64String+padding).replace(/\-/g, "+").replace(/_/g, '/');
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
    for(let i = 0; i <rawData.length;++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

export function getRandomInt(max : number) {
  return Math.floor(Math.random() * Math.floor(max));
}

export function getToken(): string {
  const tokenString = localStorage.getItem('authentication_token') || JSON.stringify({ authentication_token: ''});
  let token = JSON.parse(tokenString);
  return token.authentication_token;
}
