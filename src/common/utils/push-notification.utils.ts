import {urlBase64ToUint8Array} from "./common.utils";
import {PlayerService} from "../../services/player.service";

export async function requestNotification(setNotificationEnabled: CallableFunction, setNotificationLoading: CallableFunction) {
  if ("serviceWorker" in navigator && "PushManager" in window && "Notification" in window) {
    setNotificationLoading(true);
    const permissionResult = await Notification.requestPermission();
    if (permissionResult === "granted") {
      createNotificationSubscription()
        .then(async function (subscription) {
          await postSubscription('subscribe', subscription)
          setNotificationEnabled(true);
        })
        .catch(err => {
          console.error("Couldn't create the notification subscription", err, "name:", err.name, "message:", err.message, "code:", err.code);
        }).finally(() => setNotificationLoading(false));
    } else {
      setNotificationLoading(false);
    }

  }
}

export async function unsubscribeNotification(setNotificationEnabled: CallableFunction, setNotificationLoading: CallableFunction) {
  setNotificationLoading(true)
  const serviceWorker = await navigator.serviceWorker.ready;
  const subscription: PushSubscription | null = await serviceWorker.pushManager.getSubscription()
  if (subscription !== null) {
    const result = await postSubscription('unsubscribe', subscription);
    if (result.status === 202) {
      await subscription.unsubscribe()
    }
  }
  setNotificationLoading(false)
  setNotificationEnabled(false);
}

export async function hasNotification() {
  if ("serviceWorker" in navigator && "PushManager" in window && "Notification" in window){
    const serviceWorker = await navigator.serviceWorker.ready;
    return (await serviceWorker.pushManager.getSubscription() !== null)
  }
  return false;
}

async function postSubscription(status: string, subscription: PushSubscription) {
  const url = `${process.env.REACT_APP_API_URL}webpush/save_information/`
  const browser_match = navigator.userAgent.match(/(firefox|msie|chrome|safari|trident)/ig)
  const data = {
    status_type: status,
    subscription: subscription.toJSON(),
    browser: browser_match !== null ? browser_match[0].toLowerCase() : "",
    group: "fe-group"
  };
  const token = await PlayerService.getInstance().getToken();

  return await fetch(url, {
    credentials: "omit",
    headers: {
      "content-type": "application/json;charset=UTF-8",
      "Authentication-Token": token,
      "sec-fetch-mode": "cors"
    },
    body: JSON.stringify(data),
    method: "POST",
    mode: "cors"
  });
}

async function createNotificationSubscription() {
  //wait for service worker installation to be ready
  const serviceWorker = await navigator.serviceWorker.ready;
  // subscribe and return the subscription
  const subscription = await serviceWorker.pushManager.getSubscription();
  if (subscription) {
    return subscription;
  }
  const options = {
    userVisibleOnly: true,
    applicationServerKey: urlBase64ToUint8Array(process.env.REACT_APP_PUSH_KEY || "")
  };
  return await serviceWorker.pushManager.subscribe(options);
}