export enum GameStatus {
  Waiting = 'Waiting',
  PlayerTurn = 'PlayerTurn',
  Lost = 'Lost',
  BeingCreated = 'BeingCreated',
  Won = 'Won',
}

export enum ServerGameStatus {
  NotStarted = 'Non débutée',
  Started = 'En cours',
  Ended = 'Terminée'
}

export const serverGameStatusMap: {[key: number]: ServerGameStatus} = {
  1: ServerGameStatus.NotStarted,
  2: ServerGameStatus.Started,
  3: ServerGameStatus.Ended
}