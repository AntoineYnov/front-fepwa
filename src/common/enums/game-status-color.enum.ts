export enum GameStatusColor {
  Waiting = '#F2CC90',
  PlayerTurn = '#98F290',
  Lost = '#F29490',
  BeingCreated = '#F2F290',
  Won = '#90BDF2',
}

export enum GameStatusTagColor {
  Waiting = 'orange',
  PlayerTurn = 'cyan',
  Lost = 'red',
  BeingCreated = 'blue',
  Won = 'green',
}