import { GameComponentType } from './game-component-type.enum';
import { GameComponent } from './game-component.class';
import { Vector2D } from '../vector-2d.class';

/**
 * @class AnimationComponent
 * @classdesc
 * GameComponent to manage animation of a GameObject
 */
export class MovementComponent extends GameComponent {


    public loadAssets(): Promise<void> {
        // nothing to do
        return Promise.resolve()
    }

    type = GameComponentType.MovementComponent;
    
    public move(x: number, y: number) {
      this.gameObject!.position = new Vector2D(x,y)
    }
  }