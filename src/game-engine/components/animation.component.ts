import { loadImage } from '../../helpers/images.helper';
import { SpriteAnimation } from '../sprite-animation.class';
import { GameComponentType } from './game-component-type.enum';
import { GameComponent } from './game-component.class';

/**
 * @class AnimationComponent
 * @classdesc
 * GameComponent to manage animation of a GameObject
 */
export class AnimationComponent extends GameComponent {
  type = GameComponentType.AnimationComponent;

  // Loaded image of the used sprite sheet.
  spriteSheet: HTMLImageElement | undefined;

  // Path to the sprite sheet
  spriteSheetUri: string;

  // Current running sprite animation
  currentAnimation: SpriteAnimation;

  // Array of the animations
  animations: Array<SpriteAnimation>;

  /**
   * @constructor
   * @param spriteSheetUri 
   * @param animations 
   * @param currentAnimationIndex 
   */
  constructor(spriteSheetUri: string, animations: Array<SpriteAnimation>, currentAnimationIndex: number) {
    super();
    this.spriteSheetUri = spriteSheetUri;
    this.animations = animations;
    this.currentAnimation = animations[currentAnimationIndex];
  }

  /**
   * Load the animations images
   */
  public async loadAssets() {
    this.spriteSheet = await loadImage(this.spriteSheetUri);
  }
}