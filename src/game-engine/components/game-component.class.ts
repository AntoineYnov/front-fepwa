import { GameComponentType } from './game-component-type.enum';
import { GameObject } from '../game-object.class';

/**
 * @class GameComponent
 * @classdesc Parent class of a component wich can be attach and use
 * by GameObjects class
 */
export abstract class GameComponent {

  // Component type to check type when working with T types.
  public type: GameComponentType = GameComponentType.AbstractComponent;

  // Component type to check type when working with T types.
  public gameObject: GameObject | undefined;

  /**
   * Method to load assets of the components if necessary, like images.
   */
  public abstract loadAssets(): Promise<void>;

}