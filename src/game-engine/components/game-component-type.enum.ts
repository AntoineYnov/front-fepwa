
/**
 * @enum GameComponentType
 * Describe all the GameComponent class in an enum.
 * Used to check types using T types.
 */
export enum GameComponentType {
  AbstractComponent = 'AbstractComponent', // type for the GameObject component
  AnimationComponent = 'AnimationComponent',
  MovementComponent = 'MovementComponent',
}