import {Cell} from "./Cell.class";
import {Vector2D} from "./vector-2d.class";
import {GameObject} from "./game-object.class";

const cellsNotMovable = [
  {x: 0, y: 0},
  {x: 1, y: 0},
  {x: 2, y: 0},
  {x: 2, y: 1},
  {x: 1, y: 1},
  {x: 0, y: 1},
  {x: 0, y: 2},
  {x: 1, y: 2},
  {x: 0, y: 3},
  {x: 5, y: 3},
  {x: 5, y: 4},
  {x: 6, y: 4},
  {x: 6, y: 3},
  {x: 6, y: 5},
  {x: 7, y: 4},
  {x: 8, y: 4},
  {x: 7, y: 3},
  {x: 8, y: 3},
  {x: 9, y: 3},
  {x: 9, y: 2},
  {x: 8, y: 2},
  {x: 7, y: 2},
  {x: 6, y: 2},
  {x: 7, y: 1},
  {x: 8, y: 1},
  {x: 9, y: 1},
  {x: 9, y: 2},
  {x: 10, y: 2},
  {x: 10, y: 1},
  {x: 9, y: 3},
  {x: 11, y: 1},
  {x: 10, y: 0},
  {x: 9, y: 0},
  {x: 11, y: 0},
  {x: 12, y: 1},
  {x: 15, y: 2},
  {x: 16, y: 2},
  {x: 14, y: 1},
  {x: 15, y: 1},
  {x: 16, y: 1},
  {x: 17, y: 1},
  {x: 15, y: 0},
  {x: 16, y: 0},
  {x: 17, y: 0},
  {x: 18, y: 0},
  {x: 14, y: 4},
  {x: 15, y: 4},
  {x: 15, y: 5},
  {x: 14, y: 5},
  {x: 16, y: 5},
  {x: 14, y: 6},
  {x: 15, y: 6},
  {x: 15, y: 7},
  {x: 14, y: 10},
  {x: 13, y: 10},
  {x: 12, y: 11},
  {x: 13, y: 11},
  {x: 14, y: 11},
  {x: 14, y: 12},
  {x: 13, y: 12},
  {x: 12, y: 12},
  {x: 12, y: 13},
  {x: 13, y: 13},
  {x: 14, y: 13},
  {x: 10, y: 12},
  {x: 10, y: 13},
  {x: 9, y: 13},
  {x: 8, y: 13},
  {x: 8, y: 14},
  {x: 9, y: 14},
  {x: 9, y: 15},
  {x: 8, y: 15},
  {x: 7, y: 15},
  {x: 4, y: 9},
  {x: 4, y: 10},
  {x: 3, y: 10},
  {x: 2, y: 10},
  {x: 2, y: 10},
  {x: 1, y: 11},
  {x: 2, y: 11},
  {x: 3, y: 11},
  {x: 4, y: 11},
  {x: 5, y: 12},
  {x: 4, y: 12},
  {x: 3, y: 12},
  {x: 2, y: 12},
  {x: 1, y: 12},
  {x: 0, y: 12},
  {x: 0, y: 13},
  {x: 1, y: 13},
  {x: 2, y: 13},
  {x: 3, y: 13},
  {x: 4, y: 13},
  {x: 4, y: 14},
  {x: 3, y: 14},
  {x: 2, y: 14},
  {x: 1, y: 14},
  {x: 0, y: 14},
  {x: 0, y: 15},
  {x: 1, y: 15},
  {x: 2, y: 15},
  {x: 3, y: 15},
]

export class Grid extends GameObject{
  cell_count_x: number;
  cell_count_y: number;
  cells: Array<Cell>

  constructor(position: Vector2D, cell_count_x: number, cell_count_y: number) {
    super(position);
    this.cell_count_x = cell_count_x;
    this.cell_count_y = cell_count_y
    this.cells = []
    for (let i = 0; i < this.cell_count_x; ++i) {
      for (let j = 0; j < this.cell_count_y; ++j) {
        this.cells.push(new Cell(new Vector2D(i, j)));
      }
    }
    cellsNotMovable.forEach(val => this.getCellByPosition(new Vector2D(val.x,val.y))!.setIsMovable(false));
  }

  getCellByPosition(position: Vector2D) {
    return this.cells.find(c => c.position.x === position.x && c.position.y === position.y);
  }

  draw(ctx: CanvasRenderingContext2D, cellPixelSize: Vector2D, time: number) {
    cellPixelSize = this.getCellPixelSize(ctx);

    // draw cells stroke
    const last_color = ctx.strokeStyle
    ctx.strokeStyle = 'rgba(255,255,255,0.5)';
    this.cells.forEach((cell: Cell) => {
      ctx.strokeRect(
        cell.position.x * cellPixelSize.x,
        cell.position.y * cellPixelSize.y,
        cellPixelSize.x,
        cellPixelSize.y);
    });
    ctx.strokeStyle = last_color;

    // draw unmovable cells
    this.cells.forEach((cell: Cell) => {
      if(!cell.isMovable) {
        cell.color = 'rgba(0,0,0,0.3)';
        cell.draw(ctx, cellPixelSize, time);
      }
    })
  }

  getCellPixelSize(ctx: CanvasRenderingContext2D) {
    return new Vector2D(
      ctx.canvas.width / this.cell_count_x,
      ctx.canvas.height / this.cell_count_y
    );
  }

  update(time: number): void {
  }
}
