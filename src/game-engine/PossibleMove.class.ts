import {Cell} from "./Cell.class";
import {GameObject} from "./game-object.class";
import {Vector2D} from "./vector-2d.class";

export class PossibleMove extends GameObject {
  cells: Array<Cell>
  color: string = 'rgba(0,0,255,0.2)'

  constructor() {
    super(new Vector2D(0, 0));
    this.cells = [];
  }

  draw(ctx: CanvasRenderingContext2D, cellPixelSize: Vector2D, time: number) {
    this.cells.forEach((cell: Cell) => {
      cell.setColor(this.color);
      cell.draw(ctx, cellPixelSize, time);
    })
  }

  updatesCells(cells: Array<Cell>) {
    this.cells = [];
    this.cells = cells;
  }

  getCellByPosition(x: number, y:number): Cell|undefined {
    return this.cells.find((cell) => cell.position.x === x && cell.position.y === y);
  }

  update(time: number): void {
  }
}