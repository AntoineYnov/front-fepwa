import {Grid} from "./Grid.class";
import {Vector2D} from "./vector-2d.class";
import {GameObject} from "./game-object.class";
import {GameMap} from "./GameMap.class";
import {Player} from "../assets/player.class";
import {PossibleMove} from "./PossibleMove.class";
import {Game} from "../models/Game.class";
import {PossibleMoveCalculation} from "./PossibleMoveCalculation";
import {Cell} from "./Cell.class";
import {PossibleSmash} from "./PossibleSmash.class";
import { ApiService } from "../services/api.service";
import { GameService } from "../services/game.service";


export class GameManager {
  grid: Grid;
  map: GameMap;
  possibleMove: PossibleMove;
  possibleSmash: PossibleSmash;
  game!: Game;
  gameSlug : string;

  
  apiService : ApiService =  ApiService.getInstance();
  gameService: GameService = GameService.getInstance();

  constructor(gameSlug: string){
    this.grid = new Grid(new Vector2D(0, 0), 25, 16);
    this.map = new GameMap(new Vector2D(0,0));
    this.possibleMove = new PossibleMove();
    this.possibleSmash = new PossibleSmash();
    this.gameSlug = gameSlug;
  }

  async init() {
    this.game = await this.gameService.getGame(this.gameSlug);
  }

  receiveClick(x: number, y: number) {
    const mePlayer = this.getMePlayer();
    if (this.playerCanPlay(mePlayer)) {
      // Click on my user player
      if (mePlayer?.position.x === x && mePlayer.position.y === y) {
        this.hasClickOnMePlayer(mePlayer);
      }
      // click on possible move cell
      else if (
        mePlayer?.isSelected
        && this.possibleMove.getCellByPosition(x, y) !== undefined
      ){
        mePlayer.isSelected = false;
        this.possibleMove.updatesCells([]);
        this.possibleSmash.updatesCells([])
        mePlayer.teleportation(x, y);
      }
      // click on player to smash
      else if (
        mePlayer?.isSelected
        && this.possibleSmash.getCellByPosition(x, y) !== undefined
      ) {
        mePlayer.isSelected = false;
        this.possibleMove.updatesCells([]);
        this.possibleSmash.updatesCells([])
        const playerToSmash = this.getPlayers().find((player) => player.position.x === x && player.position.y === y);
        if(playerToSmash) {
          mePlayer.smash(playerToSmash)
          this.checkKill();
        }
      }
    }

  }

  checkKill(): void {
    this.getPlayers().forEach((player) => {
      if(player.currentPv <= 0 && player.endPosition === undefined) {
        const playersPlaying = this.getPlayers().filter((player) => player.endPosition === undefined)
        player.endPosition = playersPlaying.length;
      }
    })
  }

  playerCanPlay(mePlayer: Player|undefined): boolean {
    return mePlayer !== undefined && mePlayer.gameOrder === this.game.current_player_number_to_play;
  }

  hasClickOnMePlayer(mePlayer: Player) {
    if(!mePlayer.isSelected) {
      if (!mePlayer.hasMove)
        this.updatePossibleMove(mePlayer.position.x, mePlayer.position.y, mePlayer.character!.range);
      if (!mePlayer.hasSmash)
        this.updatePossibleSmash(mePlayer.position);
    } else {
      this.possibleMove.updatesCells([]);
      this.possibleSmash.updatesCells([])
    }
    mePlayer.isSelected = !mePlayer.isSelected;
  }

  endPlayerTurn() {
    while(true) {
      this.game.current_player_number_to_play++;
      if(this.game.current_player_number_to_play === this.getPlayers().length) {
        this.game.current_player_number_to_play = 0;
        this.game.currentTurn++;
      }
      const player: Player|undefined = this.getPlayerTurn();
      if(player === undefined || player.currentPv > 0) {
        break;
      }
    }
    this.getPlayers().forEach((player) => {player.hasSmash = false; player.hasMove = false;})
    // update in api
    this.gameService.updateGame(this.game.id, Game.toServerUpdate(this.game));
  }

  updatePossibleSmash(position: Vector2D) {
    const playerSmashable: Player[] = [];
    this.getPlayers().forEach((player) => {
      if (
        (player.position.x === position.x-1 && player.position.y === position.y && player.currentPv > 0)
        || (player.position.x === position.x+1 && player.position.y === position.y && player.currentPv > 0)
        || (player.position.x === position.x && player.position.y === position.y-1 && player.currentPv > 0)
        || (player.position.x === position.x && player.position.y === position.y+1 && player.currentPv > 0)
      ){
        playerSmashable.push(player);
      }
    })
    this.possibleSmash.updatesCells(playerSmashable.map((player) => new Cell(player.position)));
  }

  updatePossibleMove(x: number, y: number, range: number) {
    const currentCell = this.getGrid().getCellByPosition(new Vector2D(x, y))
    if (this.possibleMove && currentCell && currentCell.isMovable) {
      const possiblesCells: Array<Cell> = (new PossibleMoveCalculation()).calculateMove(
        currentCell,
        range,
        this.getGrid(),
        this.getPlayers()
      )
      this.possibleMove.updatesCells(possiblesCells);
    }
  }

  getGamesObjects(): Array<GameObject> {
    const gamesObjects: GameObject[] = [...this.getPlayers()];
    gamesObjects.push(this.possibleMove);
    return gamesObjects;
  }

  getPlayers(): Array<Player> {
    return this.game.gamePlayers;
  }

  getPossibleMove(): PossibleMove {
    return this.possibleMove;
  }

  getGrid(): Grid {
    return this.grid;
  }

  getMap(): GameMap {
    return this.map;
  }

  getGameSlug(): string {
    return this.game.slug;
  }

  getPlayerTurn(): Player|undefined {
    return this.getPlayers().find(player => player.gameOrder === this.game.current_player_number_to_play)
  }

  getMePlayer(): Player|undefined {
    return this.getPlayers().find(player => player.playerMenu !== undefined && player.playerMenu._isYou);
  }

  getPossibleSmash(): PossibleSmash {
    return this.possibleSmash;
  }

}