import {GameObject} from "./game-object.class";
import {loadImage} from "../helpers/images.helper";
import {Vector2D} from "./vector-2d.class";


export class GameMap extends GameObject {

  mapSprite: HTMLImageElement | undefined;
  spriteSheetUri: string;

  constructor(position: Vector2D) {
    super(position);
    this.spriteSheetUri = "/assets/map.png"
  }

  draw(ctx: CanvasRenderingContext2D, cellPixelSize: Vector2D, time: number) {
    ctx.imageSmoothingEnabled = false;
    if (this.mapSprite) {
      ctx.drawImage(
        this.mapSprite,
        0,
        0,
        this.mapSprite.width,
        this.mapSprite.height,
        0,
        0,
        ctx.canvas.width,
        ctx.canvas.height
      );
    }
  }

  async loadAssets(): Promise<void[]> {
    await super.loadAssets();
    this.mapSprite = await loadImage(this.spriteSheetUri)
    return []
  }

  update(time: number): void {

  }
}