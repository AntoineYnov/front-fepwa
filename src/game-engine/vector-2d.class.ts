export class Vector2D {

  x: number;
  y: number;

  constructor(x: number, y:number) {
    this.x = x;
    this.y = y;
  }

  normalize(scale: number): Vector2D {
    let x = 0;
    let y = 0;

    var norm = Math.sqrt(this.x * this.x + this.y * this.y);
    if (norm !== 0) { // as3 return 0,0 for a point of zero length
      x = scale * this.x / norm;
      y = scale * this.y / norm;
    }
    return new Vector2D(x, y);
  }

  add(vector: Vector2D): Vector2D  {
    const x = this.x + vector.x;
    const y = this.y + vector.y;

    return new Vector2D(x, y);
  }

  substract(vector: Vector2D): Vector2D  {
    const x = this.x - vector.x;
    const y = this.y - vector.y;

    return new Vector2D(x, y);
  }

  multiply(value: number): Vector2D {
    const x = this.x * value;
    const y = this.y * value;

    return new Vector2D(x, y)
  }

  equal(vector: Vector2D) {
    return this.x === vector.x && this.y === vector.y;
  }
} 