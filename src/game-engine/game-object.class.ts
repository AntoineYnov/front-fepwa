import { Vector2D } from './vector-2d.class';
import { GameComponentType } from './components/game-component-type.enum';
import { GameComponent } from './components/game-component.class';

export abstract class GameObject {

  // coordinates in a 2D space
  position: Vector2D;

  // list of the game components the game object has access
  private components: Array<GameComponent> = []

  /**
   * @constructor
   * @param {Vector2D} position base coordinate of the GameObject
   */
  constructor(position: Vector2D) {
    this.position = position;
  }

  /**
   * Contain all actions that need to be executed before
   * the render of the scene the GameObject is instance in.
   */
  async start(): Promise<void> {
    await this.loadAssets();
  }; 

  /**
   * Contain all modifications of the GameObject needed at a specific time.
   * Need to be called each frame
   * @param {number} time time passed 
   */
  abstract update(time: number): void;

  /**
   * Draw function that need a Canvas Context 2D
   * @param {CanvasRenderingContext2D} ctx context
   * @param {Vector2D} cellPixelSize the cell pixel size. Get by grid getCellPixelSize
   * @param {number} time time passed at the draw time
   */
  abstract draw(ctx: CanvasRenderingContext2D, cellPixelSize: Vector2D, time: number): void;
  
  /**
   * Attach a GameComponent to the GameObject.
   * @param {GameComponent} component new component to attach 
   */
  addComponent(component: GameComponent): void {
    component.gameObject = this;
    this.components.push(component);
  }

  /**
   * Get a GameComponent previously attached to the GameObject
   * @param {T extends GameComponent} T the expected GameComponent type
   * @param {GameComponentType} type GameComponent type
   */
  getComponent<T extends GameComponent>(type: GameComponentType): T {
    const component = this.components.find((gc) => gc.type === type );
    if (!component) throw Error('Cannot find component from GameObject');
    return component as T;
  }

  /**
   * Call the loadAssets() method of all the GameComponents.
   * Can be override to load specific assets.
   */
  async loadAssets() {
    return Promise.all(this.components.map((c) => c.loadAssets()));
  }
}