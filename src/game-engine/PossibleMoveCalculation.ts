import {Vector2D} from "./vector-2d.class";
import {Grid} from "./Grid.class";
import {Cell} from "./Cell.class";
import {Player} from "../assets/player.class";

export class PossibleMoveCalculation {
  calculateMove(start: Cell, range: number, grid: Grid, gamePlayers: Player[]): Array<Cell>  {
    let cells: Array<Cell>;
    cells = [];
    cells.push(start);
    gamePlayers = gamePlayers.filter((player) => player.currentPv >0)
    const playersPositions: Vector2D[] = gamePlayers.map((player) => player.position);
    this.getPossibleCells(cells, start, grid, range, playersPositions);

    let finalCells:Array<Cell> = [];

    // remove player cell
    cells = cells.filter((cell) => cell.position.x !== start.position.x || cell.position.y !== start.position.y);
    // remove doubles
    cells.forEach((c) => {
      if (finalCells.find(fc => c.position.x === fc.position.x && c.position.y === fc.position.y) === undefined) {
        finalCells.push(c);
      }
    })
    return finalCells;
  }

  getPossibleCells(alreadyAddedCells: Array<Cell>, currentCell: Cell, grid: Grid, deep: number, playersPositions: Vector2D[]) {
    if(deep > 0) {
      this.checkAddAndGetPossible(currentCell.position.x-1, currentCell.position.y, grid, alreadyAddedCells, deep, playersPositions);
      this.checkAddAndGetPossible(currentCell.position.x+1, currentCell.position.y, grid, alreadyAddedCells, deep, playersPositions);
      this.checkAddAndGetPossible(currentCell.position.x, currentCell.position.y-1, grid, alreadyAddedCells, deep, playersPositions);
      this.checkAddAndGetPossible(currentCell.position.x, currentCell.position.y+1, grid, alreadyAddedCells, deep, playersPositions);
    }
  }

  checkAddAndGetPossible(x: number, y:number, grid: Grid, alreadyAddedCells: Array<Cell>, deep: number, playersPositions: Vector2D[]) {
    if(playersPositions.find((pos => pos.x === x && pos.y === y)) === undefined) {
      const cell = grid.getCellByPosition(new Vector2D(x, y));
      if (cell && cell.isMovable) {
        alreadyAddedCells.push(cell);
        this.getPossibleCells(alreadyAddedCells, cell, grid, deep-1, playersPositions);
      }
    }
  }

}