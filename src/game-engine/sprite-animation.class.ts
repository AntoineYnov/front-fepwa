import { SpriteAnimationEntry, SpriteAnimationTimeline } from './sprite-animation-timeline.class';

/**
 * @class SpriteAnimation
 * Define one animation of a GameObject using an AnimationComponent
 */
export class SpriteAnimation {
  // duration of the animation in ms
  length: number;

  // evolution of the animation 
  spriteAnimationTimeline: SpriteAnimationTimeline;

  // time on the animation started
  // startTime: number;

  /**
   * @constructor
   * 
   * @param length 
   * @param spriteAnimationTimeline 
   */
  constructor(length: number, spriteAnimationTimeline: SpriteAnimationTimeline) {
    this.length = length;
    this.spriteAnimationTimeline = spriteAnimationTimeline;
  }

  /**
   * Get the current animation frame running in function of time
   * @param {number} time 
   */
  getSpriteAnimationEntryFromTime(time: number): SpriteAnimationEntry {
    return this.spriteAnimationTimeline.find((sae: SpriteAnimationEntry, i: number) => {
      const animationTime = time % this.length;
      if (i === this.spriteAnimationTimeline.length - 1) return true;
      return animationTime >= sae.time && animationTime < this.spriteAnimationTimeline[i + 1].time;
    })!;
  }
}