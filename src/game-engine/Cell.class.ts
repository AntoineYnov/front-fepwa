import {Vector2D} from "./vector-2d.class";
import {GameObject} from "./game-object.class";


export class Cell extends GameObject {
  isMovable = true;
  color = 'rgba(255,255,255,1)'

  constructor(position: Vector2D) {
    super(position);
    this.position = position;
  }

  setIsMovable(isMovable: boolean) {
    this.isMovable = isMovable;
  }

  setColor(color: string) {
    this.color = color
  }

  draw(ctx: CanvasRenderingContext2D, cellPixelSize: Vector2D, time: number): void {
    const current_color = ctx.fillStyle;
    ctx.fillStyle = this.color;
    ctx.fillRect(
      this.position.x*cellPixelSize.x,
      this.position.y*cellPixelSize.y,
      cellPixelSize.x,
      cellPixelSize.y,
      )
    ctx.fillStyle = current_color;
  }

  update(time: number): void {
  }

}
