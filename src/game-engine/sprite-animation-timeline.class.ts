/**
 * @interface SpriteAnimationTimeline
 * Array of SpriteAnimationEntry
 */
export interface SpriteAnimationTimeline extends Array<SpriteAnimationEntry> {}

/**
 * @interface SpriteAnimationEntry
 * Define one frame of an animation, at a precise animation time.
 */
export interface SpriteAnimationEntry {
  time: number; // time in animation where the sprite should be displayed
  col: number; // the column of the src sprite sheet
  row: number; // the row of the src sprite sheet
}