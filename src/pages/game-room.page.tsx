import {Button} from 'antd';
import React, { useEffect, useState } from 'react';
import {useHistory, useLocation, useParams} from "react-router-dom";
import { PlayerListItem } from '../components/player-list-item.component';
import '../styles/game-room.style.scss';
import {PlayerMenu} from "../models/PlayerMenu.class";
import {Game} from "../models/Game.class";
import {Player} from "../assets/player.class";
import { GameService } from '../services/game.service';
import { ServerGameStatus } from '../common/enums/game-status.enum';

interface IProps {
  networkOn:boolean
}

interface IState {
  mePlayer: PlayerMenu,
}

const GameRomePage: React.FC<IProps> = (props: IProps) => {

  const params = useParams<{ id: string }>();
  const history = useHistory();
  const {state}  = useLocation();
  const castedState = state as IState;

  const gameService = GameService.getInstance();

  const [game, setGame] = useState<Game>();

  async function handleStartGameClick(): Promise<void> {
    await gameService.startGame(params.id);
    history.push(`/game/${params.id}`)
  }

  async function handleCancelGameClick(): Promise<void> {
    await gameService.closeGame(params.id);
    history.push(`/`)
  }

  async function handleQuitGameClick(): Promise<void> {
    await gameService.leaveGame(params.id);
    history.push(`/`)
  }

  async function getGame() {
    try {
      const game = await gameService.getGame(params.id);
      if (game.serverState === ServerGameStatus.Started) {
        history.push(`/game/${params.id}`);
      }
      setGame(game);
    } catch (e) {
      history.push(`/error/`);
    }
  }
  if (castedState === undefined) {
    history.push(`/error/`);
  }

  useEffect(() => {
    getGame()
  }, []);

  return (
    <div className="game-room-page">
      <h1>Partie {params.id}</h1>
      <h2>Joueurs</h2>
      <div className="player-list">
        {
          game?.gamePlayers.map((player: Player) => {
            return <PlayerListItem
              key={player.playerMenu._id}
              player={player}
              isCreator={player.playerMenu._id === game.creator?._id}
              isYou={player.playerMenu._id === castedState.mePlayer._id}
            />
          })
        }
      </div>
      {castedState &&
      <>
      <Button className="actionButton" disabled={game?.creator?._id !== castedState.mePlayer._id || !props.networkOn} onClick={handleStartGameClick}>Lancer la partie</Button>
      <Button className="actionButton" disabled={game?.creator?._id !== castedState.mePlayer._id || !props.networkOn} onClick={handleCancelGameClick}>Annuler la partie</Button>
      <Button className="actionButton" disabled={game?.creator?._id === castedState.mePlayer._id || !props.networkOn} onClick={handleQuitGameClick}>Quitter la partie</Button>
      </>
      }
    </div>
  )
}

export default GameRomePage;