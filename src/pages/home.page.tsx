import { Button, Divider, Input, List, notification } from 'antd';
import React, {ChangeEvent, useEffect, useState} from 'react';
import { useHistory } from "react-router-dom";
import { GameListItem } from '../components/game-list-item.component';
import Space from '../components/space.component';
import {PlayerMenu} from "../models/PlayerMenu.class";
import {Game, IGameFromServer} from "../models/Game.class";
import {requestNotification, unsubscribeNotification} from "../common/utils/push-notification.utils";
import {PlayerService} from "../services/player.service";
import { UserInfosModal } from '../components/user-infos-modal.component';
import { PlayerMenuListItem } from '../components/player-menu-list-item';
import { ApiService } from '../services/api.service';
import { serverGameStatusMap } from '../common/enums/game-status.enum';
import {GameService} from "../services/game.service";

interface IProps {
  networkOn: boolean
}

const HomePage: React.FC<IProps> = (props: IProps) => {

  const [gameId, setGameId] = useState<string>()
  const [mePlayer, setMePlayer] = useState<PlayerMenu|null>(null)
  const [playerGames, setPlayerGames] = useState<Game[]>([])
  const [notificationEnabled, setNotificationEnabled] = useState<boolean>(false)
  const [notificationLoading, setNotificationLoading] = useState<boolean>(false)

  const [isUserInfosModalVisible, setIsUserInfosModalVisible] = useState<boolean>(false)

  const history = useHistory();

  const playerService = PlayerService.getInstance();
  const gameService = GameService.getInstance();
  
  const apiService : ApiService =  ApiService.getInstance();


  useEffect(() => {loadAPIData(); hasNotification().then(v => setNotificationEnabled(v))}, []);


  async function loadPlayerGames(mePlayer: PlayerMenu) {
    return await GameService.getInstance().getPlayerGame()
  }

  /**
   * Load the Home page informations from the API
   * @return {Promise<void>}
   */
  async function loadAPIData() {
    const localMePlayer = await playerService.getAuthenticatedPlayer();
    setMePlayer(localMePlayer);
    if(localMePlayer !== null) {
      if (!localMePlayer._pseudo) setIsUserInfosModalVisible(true);
      setPlayerGames(await loadPlayerGames(localMePlayer));
    }
  }

  /**
   * Handle the click event on the join game button
   */
  async function handleOnJoinGameClick(): Promise<void> {
    try {
      await apiService.request('post','games/join/'+ gameId)
      const game = await gameService.getGame(gameId!);

      history.push({
        pathname: `/game-room/`+game.slug,
        state: {mePlayer: mePlayer, game: game}
      })
    } catch {
      notification.error({
        message: 'Erreur',
        description:
          'Impossible de rejoindre la partie.',
      });
    }
  }

  /**
   * Handle the click event on the create game button
   */
  async function handleOnCreateGameClick(): Promise<void> {
    const gameFromAPI = await apiService.request('post','games') as IGameFromServer
    if (mePlayer != null) {
      const game: Game = new Game(gameFromAPI.id, gameFromAPI.slug, mePlayer, serverGameStatusMap[gameFromAPI.state]);
      history.push({
        pathname: `/game-room/`+game.slug,
        state: {mePlayer: mePlayer, game: game}
      })
    }
  }

  /**
   * Handle the change event on the game id input
   * @param {ChangeEvent<HTMLInputElement>} event
   */
  function handleOnGameIdChange(event: ChangeEvent<HTMLInputElement>): void {
    setGameId(event.target.value);
  }

  /**
   * Handle the on visibility change event of the User Info Modal component
   * @param {boolean} isVisible if the modal should be visible now
   */
  async function handleOnVisibilityChange(isVisible: boolean): Promise<void> {
    setIsUserInfosModalVisible(isVisible);
    const playerMenu = await playerService.getAuthenticatedPlayer();
    setMePlayer(playerMenu);
  }

  async function hasNotification() {
    if ("serviceWorker" in navigator && "PushManager" in window && "Notification" in window){
      const serviceWorker = await navigator.serviceWorker.ready;
      return (await serviceWorker.pushManager.getSubscription() !== null)
    }
    return false;
  }

  async function handleNotificationRequest() {
    requestNotification(setNotificationEnabled, setNotificationLoading)
  }

  async function handleUnsubscribeNotification() {
    unsubscribeNotification(setNotificationEnabled, setNotificationLoading)
  }


  return (
    <div className="home">
      <div className="notification-block">
        {"Notification" in window && !notificationEnabled && <Button loading={notificationLoading} disabled={!props.networkOn} type="primary" onClick={handleNotificationRequest}>Recevoir les notifications</Button>}
        {"Notification" in window && !notificationEnabled && <p>Les notifications vous permettent d'être alerté lorsque votre tour de jouer survient.</p>}
        {notificationEnabled && <Button type="primary" loading={notificationLoading} disabled={!props.networkOn} onClick={handleUnsubscribeNotification}>Ne plus recevoir les notifications</Button>}

      </div>
      <UserInfosModal handleOnVisibilityChange={handleOnVisibilityChange} isVisible={isUserInfosModalVisible} />
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center'}}>
        { mePlayer && <PlayerMenuListItem
            playerMenu={mePlayer}
            networkOn={props.networkOn}
            editable={true}
            handleOnEdit={() => setIsUserInfosModalVisible(true)}
        /> }
      </div>
      <Divider/>
      <h2>Game ID</h2>
      <Input onChange={handleOnGameIdChange} value={gameId}/>
      <Space height={12}/>
      <Button
        onClick={handleOnJoinGameClick}
        style={{width: '100%'}}
        disabled={!gameId || !props.networkOn}
      >Rejoindre une partie</Button>
      <Divider/>
      <Button
        onClick={handleOnCreateGameClick} style={{width: '100%'}}
        disabled={mePlayer === null || !props.networkOn}
      >Créer une partie</Button>
      <Divider/>

      <h2>Mes Parties</h2>

      <List
        dataSource={playerGames}
        renderItem={item => <GameListItem mePlayer={mePlayer} game={item}/>}
      />
    </div>
  )
}

export default HomePage;