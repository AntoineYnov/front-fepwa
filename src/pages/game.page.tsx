import React, { useEffect, useState } from 'react';
import {Scene} from '../components/scene.component';
import {GameManager} from "../game-engine/GameManager";
import {useHistory, useParams} from "react-router-dom";

interface IProps {
  networkOn: boolean
}



const GamePage: React.FC<IProps> = (props: IProps) => {
  
  const [gameManager, setGameManager] = useState<GameManager>()

  const params = useParams<{ id: string }>();
  const history = useHistory();

  async function loadGameManager() {
    const gameSlug = params.id;
    try {
      const gameManager: GameManager = new GameManager(gameSlug) // will load the game;
      await gameManager.init()
      setGameManager(gameManager)
    } catch (e) {
      history.push(`/error/`);
    }

  } 
  useEffect(() => {
    loadGameManager()
  }, []);

  return (
    <div>
      {
        gameManager &&
        <Scene name="main" gameManager={gameManager} networkOn={props.networkOn}/>
      }
    </div>
  )
}

export default GamePage;