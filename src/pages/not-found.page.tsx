import React from 'react';
import { Result, Button } from 'antd';
import { useHistory } from "react-router-dom";

interface IProps {

}

const HomePage: React.FC<IProps> = (props: IProps) => {
  const history = useHistory();

  function handleReturnHomeClick(): void {
    history.push('/')
  }

  return (
    <div>
      <Result
        status="warning"
        title="There are some problems with your operation."
        extra={
          <Button type="primary" key="console" onClick={handleReturnHomeClick}>
            Return to Home
          </Button>
        }
      />
    </div>
  )
}

export default HomePage;