import React from 'react';
import { CrownFilled } from '@ant-design/icons';

import '../styles/player-list-item.style.scss';
import { PlayerMenu } from '../models/PlayerMenu.class';
import { Button } from 'antd';


interface IProps {
  playerMenu: PlayerMenu
  isCreator?: boolean,
  isYou?: boolean,
  editable?: boolean,
  handleOnEdit?: () => void,
  networkOn: boolean
}

export const PlayerMenuListItem: React.FC<IProps> = (props: IProps) => {
  
  return (
    <div className="player-list-item">
      <img
        className="player-img"
        alt={props.playerMenu._pseudo}
        src={props.playerMenu.picture_url}
        style={{
          borderColor: props.isYou ? 'orange' : 'transparent'
        }}
      />
      <p
        style={{
          color: props.isYou ? 'orange' : 'black'
        }}
      >{props.playerMenu._pseudo}</p>
      { props.editable && <Button disabled={!props.networkOn} size={'small'} onClick={props.handleOnEdit} >Modifier</Button> }
      {props.isCreator && <CrownFilled className="is-creator-icon" />}
    </div>
  )
}