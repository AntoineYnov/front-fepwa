import React, {useEffect, useRef, useState} from 'react';
import {Vector2D} from "../game-engine/vector-2d.class";
import {GameManager} from "../game-engine/GameManager";
import GamePlayerState from "./game-player-state.component";
import GameInformation from "./game-information.component";
import GamePlayerEnd from "./game-player-end.component";
import GameEnd from "./game-end.component";

interface IProps {
  height: number;
  width: number;
  gameManager: GameManager;
  networkOn: boolean;
}

/**
 * Component that use a canvas wich implement a game loop, to run code at each frames
 */
const Canvas: React.FC<IProps> = (props: IProps) => {
  const [gameManager, setGameManager] = useState<GameManager>(props.gameManager)


  const canvasRef: React.Ref<HTMLCanvasElement> = useRef<HTMLCanvasElement>(null);
  let time = 0;
  let oldTimeStamp = 0;

  const init = async (canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D) => {
  }

  /**
   * Redraw all contents that should be displayed in the canvas
   * @param {CanvasRenderingContext2D} ctx canvas context 2D
   * @param {number} time time passed since start in ms
   */
  const draw = (ctx: CanvasRenderingContext2D, time: number) => {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
    const cellPixelSize = gameManager.getGrid().getCellPixelSize(ctx);

    gameManager.getMap().draw(ctx, cellPixelSize, time);
    gameManager.getGrid().draw(ctx, new Vector2D(0, 0), time);
    gameManager.getPlayers().forEach((go) => go.draw(ctx, cellPixelSize, time));
    gameManager.getPossibleMove().draw(ctx, cellPixelSize, time);
    gameManager.getPossibleSmash().draw(ctx, cellPixelSize, time);
    drawTextInformation(ctx, cellPixelSize, time);
  }


  const drawTextInformation = (ctx: CanvasRenderingContext2D, cellSize: Vector2D, time: number) => {
    const textFontSize = 16;
    ctx.save();
    ctx.font = textFontSize + "px Arial";
    ctx.fillStyle = 'white';
    ctx.shadowColor = "black";
    ctx.shadowBlur = 3;
    ctx.fillText(`Time : ${Math.floor(time / 100) / 10}`, 8, textFontSize);
    ctx.restore();
  }

  /**
   * Called at each frames
   * @param {CanvasRenderingContext2D} ctx canvas context 2D
   * @param {number} timeStamp now time in ms
   */
  const update = async (ctx: CanvasRenderingContext2D, timeStamp: number = 0) => {
    time = timeStamp - oldTimeStamp;
    draw(ctx, time);

    window.requestAnimationFrame((timeStamp: number | undefined) => update(ctx, timeStamp));
  }

  useEffect(() => {
    const canvas = canvasRef.current!;
    const context = canvas.getContext('2d')!;
    init(canvas, context);
    let anim = window.requestAnimationFrame((timeStamp: number | undefined) => update(context, timeStamp));

    return () => {
      cancelAnimationFrame(anim);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  function clickEvent(p1: React.MouseEvent<HTMLCanvasElement>) {
    const context = p1.currentTarget.getContext('2d');
    if (context) {
      const cell_size = gameManager.getGrid().getCellPixelSize(context);

      const position_x_clicked = Math.floor((p1.clientX - p1.currentTarget.getBoundingClientRect().x) / cell_size.x)
      const position_y_clicked = Math.floor((p1.clientY - p1.currentTarget.getBoundingClientRect().y) / cell_size.y)
      gameManager.receiveClick(position_x_clicked, position_y_clicked);
      setGameManager(Object.assign(Object.create(Object.getPrototypeOf(gameManager)), gameManager))
    }

  }

  return (
    <div>
      {gameManager.game.isFinish() && <GameEnd gamePlayers={gameManager.getPlayers()}/>}
      {
        !gameManager.game.isFinish()
        && gameManager.getMePlayer()?.endPosition !== undefined
        && <GamePlayerEnd gamePlayer={gameManager.getMePlayer()}/>
      }
      <GameInformation networkOn={props.networkOn} gameManager={gameManager}/>
      <div id="canvas-container">
        <canvas
          onClick={clickEvent}
          width={props.width} height={props.height} ref={canvasRef}/>
      </div>
      <GamePlayerState gameManager={gameManager}/>
    </div>

  )
}

export default Canvas;