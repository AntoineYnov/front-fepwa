import {Button, List, Tag} from 'antd';
import React from 'react';
import {GameStatusTagColor} from '../common/enums/game-status-color.enum';
import {Game} from "../models/Game.class";
import {GameStatus} from "../common/enums/game-status.enum";
import {PlayerMenu} from "../models/PlayerMenu.class";
import {useHistory} from "react-router-dom";

interface IProps {
  mePlayer: PlayerMenu|null,
  game: Game
}

export const GameListItem: React.FC<IProps> = (props: IProps) => {

  const history = useHistory();

  function handleRoomGameClick(): void {
    if (props.mePlayer !== null && props.game.slug !== undefined) {
      history.push({
        pathname: `/game-room/`+props.game.slug,
        state: {mePlayer: props.mePlayer, game: props.game}
      })
    }
  }
  function handlePlayGameClick(): void {
    if (props.mePlayer !== null && props.game.slug !== undefined) {
      history.push( `/game/`+props.game.slug);
    }
  }

  /**
   * Get color from a game status
   * @return {string} a string hex color
   */
  function getColorFromStatus(): string {
    return GameStatusTagColor[props.game.state];
  }

  return (
    <List.Item className="game-list-item">
      <div className="info-line">
        <p>{props.game.slug}</p>
        <p>{props.game.gamePlayers.length} Joueurs</p>

      </div>
      <div className="game-status-block">
        {props.game.playerEndPlayingGame() && <Button className="game-action-button" type="primary" onClick={handlePlayGameClick}>Show result</Button>}
        {props.game.state === GameStatus.PlayerTurn && <Button className="game-action-button" type="primary" onClick={handlePlayGameClick}>Play</Button>}
        {props.game.state === GameStatus.Waiting && <Button className="game-action-button" type="primary" onClick={handlePlayGameClick}>Enter</Button>}
        {props.game.state === GameStatus.BeingCreated && <Button className="game-action-button" type="primary" onClick={handleRoomGameClick}>Join</Button>}
        <Tag className="game-status" color={getColorFromStatus()}>{props.game.state}</Tag>
      </div>
    </List.Item>
  )
}