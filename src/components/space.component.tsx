import React from 'react'

interface IProps {
  height: string | number
}

const Space: React.FC<IProps> = (props: IProps) => {
  return (
    <div style={{ width: "100%", height: props.height }}></div>
  )
}

export default Space;