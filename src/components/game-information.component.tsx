import {Button} from 'antd';
import {GameManager} from "../game-engine/GameManager";
import {useHistory} from "react-router-dom";
import React, {useEffect, useState} from "react";

interface IProps {
  gameManager: GameManager;
  networkOn: boolean;
}

const GameInformation: React.FC<IProps> = (props: IProps) => {
  let [canEndTurn, setCanEndTurn] = useState<boolean>(true);
  const history = useHistory();

  function handleGoHome(): void {
    history.push(`/`)
  }
  function handleTurnEnd(): void {
    props.gameManager.endPlayerTurn();
    setCanEndTurn(false);
  }

  useEffect(() => {
    setCanEndTurn(props.gameManager.playerCanPlay(props.gameManager.getMePlayer()));
  }, [props.gameManager])

  return (
    <div id="game-information">
      <div id="informations">
        <div>Game ID : {props.gameManager.getGameSlug()}</div>
        <div>
          Player Turn : {props.gameManager.getPlayerTurn()?.playerMenu._pseudo}
          {props.gameManager.getPlayerTurn()?.playerMenu._isYou && "(is you)"}
        </div>
      </div>
      <Button type="primary" onClick={handleGoHome}>Back to Home</Button>
      <Button onClick={handleTurnEnd} disabled={!canEndTurn || !props.networkOn}>End your turn</Button>
    </div>
  )
}

export default GameInformation;