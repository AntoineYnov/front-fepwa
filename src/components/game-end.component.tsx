import React, {useEffect, useState} from 'react'
import { Button } from 'antd';
import {useHistory} from "react-router-dom";
import {Player} from "../assets/player.class";

interface IProps {
  gamePlayers: Player[]; // all players with is finish order
}

/**
 * The game is finish
 */
const GameEnd: React.FC<IProps> = (props: IProps) => {
  const history = useHistory();
  const [gamePlayer, setGamePlayer] = useState<Player[]>(props.gamePlayers)

  useEffect(() => {
    const playersOrdered = gamePlayer.sort((p1, p2) => {
      if (p1!.endPosition !== undefined && p2!.endPosition !== undefined)
        return p1.endPosition < p2.endPosition ? 1 : -1;
      return 1
    })
    setGamePlayer([...playersOrdered])
  }, []);

  function handleGoHome(): void {
    history.push(`/`)
  }

  return (
    <div id="game-end">
      <div>
        <p>The game is finished</p>
        <ul>
          {
            gamePlayer.map((player) => {
              return (<li key={player.gameOrder}>{player?.playerMenu?._pseudo} - {player?.endPosition ?? 1}</li>)
            })
          }
        </ul>
        <Button type="primary" onClick={handleGoHome}>Back to Home</Button>
      </div>
    </div>
  )
}

export default GameEnd;