import React, { useEffect, useState } from 'react';
import Canvas from './canvas.component';
import {GameManager} from "../game-engine/GameManager";

interface IProps {
  name: string;
  gameManager: GameManager;
  networkOn: boolean;
}

export const Scene: React.FC<IProps> = (props: IProps) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const start = async (): Promise<void> => {
    setIsLoading(true);
    await Promise.all(props.gameManager.getGamesObjects().map((go) => go.start()));
    await props.gameManager.getMap().start();
    setIsLoading(false);
  }

  useEffect(() => {
    start();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  return (
    <>
      {isLoading && <p>Loading scene {props.name}...</p>}
      {!isLoading && <Canvas
          height={512}
          width={800}
          gameManager={props.gameManager}
          networkOn={props.networkOn}
      />}
    </>
  )
}