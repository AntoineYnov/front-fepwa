import React from 'react';
import { CrownFilled } from '@ant-design/icons';

import '../styles/player-list-item.style.scss';
import {Player} from "../assets/player.class";


interface IProps {
  player: Player
  isCreator?: boolean,
  isYou?: boolean,
}

export const PlayerListItem: React.FC<IProps> = (props: IProps) => {
  
  return (
    <div className="player-list-item">
      <img
        className="player-img"
        alt={props.player.playerMenu._pseudo}
        src={props.player.playerMenu.picture_url}
        style={{
          borderColor: props.isYou ? 'orange' : 'transparent'
        }}
      />
      <p
        style={{
          color: props.isYou ? 'orange' : 'black'
        }}
      >{props.player.playerMenu._pseudo}</p>
      {props.isCreator && <CrownFilled className="is-creator-icon" />}
    </div>
  )
}