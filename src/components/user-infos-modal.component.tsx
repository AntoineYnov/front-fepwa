import React from "react";
import { Modal, Form, Input, Button } from "antd";
import { PlayerService } from "../services/player.service";

interface Props {
  isVisible: boolean,
  handleOnVisibilityChange: (isVisible: boolean) => void,
}

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export const UserInfosModal: React.FC<Props> = (props: Props) => {

  const playerService = PlayerService.getInstance();

  /**
   * Handle the submit button of the form
   * Automatically close the form on success
   * @param values 
   */
  const onFinish = async (values: { pseudo: string, picture_url: string }) => {
    console.log('Success:', values);
    await playerService.updatePlayer(values);
    props.handleOnVisibilityChange(false);
  };

  return (
    <Modal title="Infos personnels" visible={props.isVisible} footer={false} closable={false}>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          label="Pseudo"
          name="pseudo"
          rules={[{ required: true, message: 'Veuillez remplir ce champ.' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Image de profile"
          name="picture_url"
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Enregistrer
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}