import React from 'react'
import { Button } from 'antd';
import {useHistory} from "react-router-dom";
import {Player} from "../assets/player.class";

interface IProps {
  gamePlayer: Player | undefined;
}

/**
 * The player has loos
 */
const GamePlayerEnd: React.FC<IProps> = (props: IProps) => {
  const history = useHistory();

  function handleGoHome(): void {
    history.push(`/`)
  }

  return (
    <div id="game-player-end">
      <div>
        <p>You lost. You are {props.gamePlayer?.endPosition}</p>
        <Button type="primary" onClick={handleGoHome}>Back to Home</Button>
      </div>
    </div>
  )
}

  export default GamePlayerEnd;