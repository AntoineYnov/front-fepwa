import React from 'react'
import {GameManager} from "../game-engine/GameManager";

import {CSSProperties, useEffect} from "react";

interface IProps {
  gameManager: GameManager;
}

const GamePlayerState: React.FC<IProps> = (props: IProps) => {
  let classWidth: CSSProperties = {};

  useEffect(() => {
    updateClassWidth();
  }, [props.gameManager])

  const updateClassWidth = () => {
    let lifeWidth = {};
    const mePlayer = props.gameManager.getMePlayer();
    if (mePlayer !== undefined) {
      lifeWidth = {
        width: mePlayer.currentPv * 100 / mePlayer.character!.hp + "%"
      };
    } else {
      lifeWidth = {
        width: "100%"
      };
    }
    classWidth = lifeWidth;
  }

  updateClassWidth()

  return (
    <div id="game-player-state">
      <div id="game-player-state-life" style={classWidth}>
        LP : {props.gameManager.getMePlayer()?.currentPv} / {props.gameManager.getMePlayer()?.character!.hp} - ATT
        : {props.gameManager.getMePlayer()?.character!.attack}
      </div>

    </div>
  )

}

export default GamePlayerState;