import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom'
import { Button, Layout, Tag } from 'antd';

import './App.scss';
import GamePage from './pages/game.page';
import HomePage from './pages/home.page';
import GameRoomPage from './pages/game-room.page';
import NotFoundPage from './pages/not-found.page';
import { PlayerService } from './services/player.service';
import { isEmpty } from './common/utils/common.utils';

const { Header, Content } = Layout;

const playerService = PlayerService.getInstance();

function App() {

  const [isLoading, setIsLoading] = useState(false)
  const [isConnected, setIsConnected] = useState(false)
  const [networkOn, setNetworkOn] = useState(false)

  window.addEventListener('offline', function() { setNetworkOn(window.navigator.onLine); });
  window.addEventListener('online', function() { setNetworkOn(window.navigator.onLine); });

  function handleOnRefreshPress() {
    window.location.reload();
  }


  useEffect(() => {
      (async() => {
          setIsLoading(true)
          setNetworkOn(window.navigator.onLine);
          let token = await playerService.getToken();
          setIsConnected(!isEmpty(token))
          setIsLoading(false)
      })()
  }, [])

    return (
        <>
            {isLoading && <p>Chargement en cours ...</p>}
            {!isLoading && !isConnected && <NotFoundPage/>}
            {!isLoading && isConnected && 
              <div className="App"> 
                <BrowserRouter>
                  <Layout className="">
                    <Header className="header">
                      <NavLink to="/">FE PWA</NavLink>
                      <Button onClick={handleOnRefreshPress} size="small" style={{ marginLeft: '8px' }} disabled={!networkOn}>Rafraîchir</Button>
                      {networkOn && <Tag className="network-information" color="green">Network enabled</Tag>}
                      {!networkOn && <Tag className="network-information" color="red">Network disabled</Tag>}
                    </Header>
                    <Content className="content">
                      <Switch>
                        <Route path="/" exact component={() => <HomePage networkOn={networkOn}/>}/>
                        <Route path="/game/:id" component={() => <GamePage networkOn={networkOn}/>}/>
                        <Route path="/game-room/:id" component={() => <GameRoomPage networkOn={networkOn}/>}/>
                        <Route path="*" component={NotFoundPage}/>
                      </Switch>
                    </Content>
                  </Layout>
                </BrowserRouter>
              </div>
            }
        </>
    )
}

export default App;
