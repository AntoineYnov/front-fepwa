export interface ICharacterFromServer {
    attack_level: number,
    defense_level: number,
    base_pv: number,
    distance_move: number,
}

export class Character {
    attack: number  = 0 ;
    defense: number = 0;
    hp  : number = 0;
    range : number = 0 ;

    constructor(attack: number, defense: number, hp: number, range: number) {
        this.attack = attack;
        this.defense = defense;
        this.hp = hp;
        this.range = range
    }

    public static fromServer(characterFromServer: ICharacterFromServer) {
        return new Character(
            characterFromServer.attack_level,
            characterFromServer.defense_level,
            characterFromServer.base_pv,
            characterFromServer.distance_move,
        )
    }

    public static toServer(character: Character) {
        return {
            attack_level: character.attack,
            defense_level: character.defense,
            base_pv: character.hp,
            distance_move: character.range,
        }
    }
}