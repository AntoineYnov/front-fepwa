export class Item {
  attack: number = 0;
  defense: number = 0;
  picture_url: string = "";

  constructor(attack: number, defense: number, picture_url: string) {
    this.attack = attack;
    this.defense = defense;
    this.picture_url = picture_url
  }
}