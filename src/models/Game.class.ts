import {Player} from "../assets/player.class";
import {GameItem} from "../assets/game-item.class";
import {IPlayerFromServer, PlayerMenu} from "./PlayerMenu.class";
import {GameStatus, ServerGameStatus, serverGameStatusMap} from "../common/enums/game-status.enum";
import {Character, ICharacterFromServer} from "./Character.class";
import {Vector2D} from "../game-engine/vector-2d.class";
import {AnimationComponent} from "../game-engine/components/animation.component";
import {SpriteAnimation} from "../game-engine/sprite-animation.class";
import {MovementComponent} from "../game-engine/components/movement.component";
import {PlayerService} from "../services/player.service";
import {GameService, IGameUpdateBody} from "../services/game.service";

export interface IPlayerGameFromServer {
  id: number,
  player: IPlayerFromServer,
  game: number,
  x: number,
  y: number,
  lasting_pv: number,
  character?: ICharacterFromServer,
  position_in_game: number,
}

export interface IGameFromServer {
  id: number,
  state: number,
  slug: string,
  creator: IPlayerFromServer,
  tour_number: number,
  player_number: number,
  player_games: IPlayerGameFromServer[],
}

export interface IGameStatsFromServer extends Array<IGamePlayerStatFromServer> {};

export interface IGamePlayerStatFromServer {
  id: number,
  player: IPlayerFromServer,
  game: number,
  final_position: number,
}

export class Game {
  id: number;
  state: GameStatus; // GameStatus.x
  serverState: ServerGameStatus; // ServerGameSatus.x
  slug: string; // game ID
  creator: PlayerMenu|null;
  currentTurn: number; // 1 turn = All player play
  current_player_number_to_play: number; // value of player order to play. Start to 1
  gamePlayers: Player[];
  gameItems: GameItem[];

  playerService = PlayerService.getInstance();
  gameService = GameService.getInstance();

  constructor(
    id: number,
    slug: string,
    creator: PlayerMenu|null,
    serverState: ServerGameStatus,
    gamePlayers: Player[] = [],
    current_player_number_to_play: number = 0,
    currentTurn: number = 0,
    gameItems: GameItem[] = []
  ) {
    this.id = id;    
    this.slug = slug;
    this.creator = creator;
    this.gamePlayers = gamePlayers;
    this.serverState = serverState;
    this.currentTurn = currentTurn;
    this.current_player_number_to_play = current_player_number_to_play;
    this.gameItems = [];
    this.gameItems = gameItems;
    this.state = GameStatus.BeingCreated;
  }

  public static async getGameStatus(serverGameStatus: ServerGameStatus, game: Game) {
    if(serverGameStatus === ServerGameStatus.NotStarted) {
      return GameStatus.BeingCreated
    }
    const meGamePlayer = game.gamePlayers.find((gamePlayer) => gamePlayer.playerMenu._isYou);
    if(serverGameStatus === ServerGameStatus.Ended) {
      return (meGamePlayer !== undefined && meGamePlayer.endPosition !== undefined)
        ? GameStatus.Lost : GameStatus.Won;
    }
    if(serverGameStatus === ServerGameStatus.Started) {
      if(meGamePlayer !== undefined && game.current_player_number_to_play === meGamePlayer.gameOrder) {
        return GameStatus.PlayerTurn ;
      }
      if(meGamePlayer !== undefined && meGamePlayer.currentPv <=0) {
        return GameStatus.Lost;
      }
    }
    return GameStatus.Waiting;
  }

  public static async fromServer(gameFromServer: IGameFromServer): Promise<Game> {
    const stats = await GameService.getInstance().getGameStats(gameFromServer.slug);
    const game = new Game(
      gameFromServer.id,
      gameFromServer.slug,
      PlayerMenu.fromServer(gameFromServer.creator),
      serverGameStatusMap[gameFromServer.state],
      await Promise.all(gameFromServer.player_games.map((player_game) => Game.getPlayerGameObjectFromServer(player_game, stats))),
      gameFromServer.player_number,
      gameFromServer.tour_number,
    )
    game.state = await Game.getGameStatus(serverGameStatusMap[gameFromServer.state], game);
    return game;
  }

  public static toServerUpdate(game: Game): IGameUpdateBody {
    return {
      player_number: game.current_player_number_to_play,
      tour_number: game.currentTurn,
      player_games_update: game.gamePlayers.map((player: Player) => ({
        id: player.playerGameId,
        x: player.position.x,
        y: player.position.y,
        player: player.playerMenu._id,
        game: game.id,
        lasting_pv: player.currentPv,
        position_in_game: player.gameOrder,
        character: Character.toServer(player.character!),
      }))
    }
  }

  isFinish(): boolean {
    return this.serverState === ServerGameStatus.Ended;
  }

  playerEndPlayingGame(): boolean {
    return this.state === GameStatus.Won || this.state === GameStatus.Lost
  }

  /**
   * Transform a player game server object into a Player GameObject
   * @param {IPlayerGameFromServer} playerGameFromServer server informations describing the player state in game
   * @param {IGameStatsFromServer} stats the state sticked in the server
   * @return {Player} a player game object instance
   */
  private static async getPlayerGameObjectFromServer(playerGameFromServer: IPlayerGameFromServer, stats: IGameStatsFromServer): Promise<Player> {
    const mePlayer = await PlayerService.getInstance().getAuthenticatedPlayer();
    const playerStat = stats.find(playerStat => playerStat.player.id === playerGameFromServer.player.id);
    const player = new Player(
      new Vector2D(playerGameFromServer.x, playerGameFromServer.y),
      PlayerMenu.fromServer(playerGameFromServer.player, mePlayer._id === playerGameFromServer.player.id),
      playerGameFromServer.id,
      playerGameFromServer.character ? Character.fromServer(playerGameFromServer.character) : undefined,
      playerGameFromServer.position_in_game,
      playerStat?.final_position,
    )

    player.currentPv = playerGameFromServer.lasting_pv;
    
    // Default Sprite sheet. Change with the character's sprite sheet later.
    const animationComponent = new AnimationComponent(
      '/assets/eirika-sprite-sheet.png',
      [
        new SpriteAnimation(2000, [
          { time: 0, col: 3, row: 0 },
          { time: 500, col: 3, row: 1 },
          { time: 1000, col: 3, row: 2 },
          { time: 1500, col: 3, row: 1 },
        ])
      ],
      0,
    );

    player.addComponent(animationComponent);
    player.addComponent(new MovementComponent());

    return player;
  }

}