export interface IPlayerFromServer {
    id: number,
    pseudo: string,
    picture_url: string,
}

export class PlayerMenu {
    _id: number  = 0 ;
    _pseudo: string = "";
    _picture_url: string = "";
    _isYou: boolean = false;

    constructor(id: number,pseudo: string, isYou: boolean = false, pictureUrl: string = '') {
        this._id = id;
        this._pseudo = pseudo;
        this._isYou = isYou;
        this._picture_url = pictureUrl;
    }

    public static fromServer(playerFromServer: IPlayerFromServer, isYou: boolean = false) {
        return new PlayerMenu(
            playerFromServer.id,
            playerFromServer.pseudo,
            isYou,
            playerFromServer.picture_url,
        )
    }

    public get picture_url() {
        return this._picture_url;
    }

    public set picture_url(picture_url: string) {
        this._picture_url = picture_url;
    }
}