# Getting Started with FE-PWA

## Members
Corentin DUPONT, Dany CORBINEAU, Pierre CHENE, Antoine GENOUIN DUHAMEL

## Manage with YARN
### Start the project

* Clone the project : `git clone https://gitlab.com/AntoineYnov/front-fepwa.git`
* Go into the directory : `cd front-fepwa`
* Install node dependencies : `yarn install`
* Copy env vars : `cp .env.sample .env`
* Start the project : `yarn start`
* Access to url : `localhost:3000`

### Update the project

* Update git : `git pull`
* Install node dependencies : `yarn install`
* Start the project : `yarn start`
* Access to url : `localhost:3000`

### Test the project

* Go to develop branche : `git checkout develop`
* Install node dependencies : `yarn install`
* Start the project : `yarn start`
* Access to url : `localhost:3000`

## Cache policy

About cache strategy:
* Cache First strategy for :
  * All assets files that are determined by their extension. (.png, .css, .js etc...)
  * All images and logo
  * All html pages
* Network First strategy for :
  * All call to `/api` url 
  
## Test the game

* https://fepwa.bartradingx.com/
